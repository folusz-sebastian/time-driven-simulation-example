package sim.core;

import sim.monitors.MonitoredVar;

public class TrafficLightOn extends SimStep {
    public MonitoredVar state = new MonitoredVar();
    private double lightGreenInterval = 4.0;
    private double lightGreenTime = 0.0;
    private TrafficLight trafficLight;


    public TrafficLightOn(Manager mngr, TrafficLight trafficLight) {
        super(mngr);
        state.setValue(0.0, simTime());
        this.trafficLight = trafficLight;
    }

    @Override
    public void stateChange() {
        if (simTime() == lightGreenTime) {
            lightGreenTime += lightGreenInterval;
            trafficLight.setLightOn(true);
            state.setValue(1, simTime());
            System.out.println("włączam światło simTime: " + simTime());
        } else {
            state.setValue(0, simTime());
        }
    }
}
