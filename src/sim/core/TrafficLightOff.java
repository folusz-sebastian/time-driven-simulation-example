package sim.core;

import sim.monitors.MonitoredVar;

public class TrafficLightOff extends SimStep {
    public MonitoredVar state = new MonitoredVar();
    private TrafficLight trafficLight;
    private double endOfLightGreenTime = 3.0;
    private double endOfLightGreenInterval = 4.0;


    public TrafficLightOff(Manager mngr, TrafficLight trafficLight) {
        super(mngr);
        state.setValue(0.0, simTime());
        this.trafficLight = trafficLight;
    }

    @Override
    public void stateChange() {
        if (simTime() == endOfLightGreenTime) {
            endOfLightGreenTime += endOfLightGreenInterval;
            trafficLight.setLightOn(false);
            state.setValue(1, simTime());
            System.out.println("wyłączam światło simTime: " + simTime());
        } else {
            state.setValue(0, simTime());
        }
    }
}
