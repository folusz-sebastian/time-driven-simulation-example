package sim.core;

import sim.monitors.MonitoredVar;
import sim.random.SimGenerator;

public class CrossingRoad extends SimStep {
    public MonitoredVar state = new MonitoredVar();
    private SimGenerator rnd = new SimGenerator();
    private TrafficLight trafficLight;
    private int maxNumberOfPeople;


    public CrossingRoad(Manager mngr, TrafficLight trafficLight, int maxNumberOfPeople) {
        super(mngr);
        state.setValue(0.0, simTime());
        this.trafficLight = trafficLight;
        this.maxNumberOfPeople = maxNumberOfPeople;
    }

    @Override
    public void stateChange() {
        int randomNumber = rnd.uniformInt(100);
        if (trafficLight.isLightOn()) {
            if (randomNumber < maxNumberOfPeople) {
                state.setValue(1, simTime());
                System.out.println(randomNumber + " ludzi przechodzi przez ulicę");
            }
            else {
                state.setValue(0, simTime());
                System.out.println("Wystąpiła awaria. Nie będzie można przechodzić przez ulicę");
                unregisterSimStep(this);
            }
        }
    }
}
