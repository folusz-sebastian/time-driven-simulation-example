package sim.core;

public class TrafficLight {
    private boolean lightOn = false;

    public boolean isLightOn() {
        return lightOn;
    }

    public void setLightOn(boolean lightOn) {
        this.lightOn = lightOn;
    }
}
