package sim;

import sim.core.*;
import sim.monitors.Diagram;

import java.awt.*;

public class Main {
    private static final double startTime = 0.0;
    private static final double timeStep = 1.0;
    private static final double endSimTime = 12.0;

    public static void main(String[] args) {
        Manager simMngr = Manager.getInstance(startTime, timeStep);

        TrafficLight trafficLight = new TrafficLight();
        TrafficLightOn trafficLightOn = new TrafficLightOn(simMngr, trafficLight);
        TrafficLightOff trafficLightOff = new TrafficLightOff(simMngr, trafficLight);
        CrossingRoad crossingRoad = new CrossingRoad(simMngr, trafficLight, 90);

        simMngr.setEndSimTime(endSimTime);
        simMngr.startSimulation();

        Diagram dOne = new Diagram(Diagram.DiagramType.TIME_FUNCTION, "Zmiany stanow");
        dOne.add(trafficLightOn.state, Color.GREEN);
        dOne.add(trafficLightOff.state, Color.RED);
        dOne.add(crossingRoad.state, java.awt.Color.BLUE);
        dOne.show();

    }
}
